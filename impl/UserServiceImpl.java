package com.yeleven.service.impl;

import com.yeleven.dao.UserDao;
import com.yeleven.dao.impl.UserDaoImpl;
import com.yeleven.pojo.User;
import com.yeleven.service.UserService;

/**
 * @author y_eleven
 * @Date 2020/7/22 12:42
 */
public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();

    @Override
    public void registUser(User user) {
        userDao.saveUser(user);
    }

    @Override
    public User login(User user) {
        return userDao.queryUserByUsernamePassword(user.getUsername(),user.getPassword());
    }

    @Override
    public boolean existsUsername(String username) {
        if(userDao.queryUserByUsername(username)==null){
            //等于null，说明没查到，没查到表示可用
            return false;
        }
        return true;
    }
}
