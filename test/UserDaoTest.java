package com.yeleven.test;

import com.yeleven.dao.UserDao;
import com.yeleven.dao.impl.UserDaoImpl;
import com.yeleven.pojo.User;
import org.junit.Test;

public class UserDaoTest {
    UserDao userDao = new UserDaoImpl();
    @Test
    public void queryUserByUsername() {

        if(userDao.queryUserByUsername("y_eleven")==null){
            System.out.println("用户名可用！");
        } else {
            System.out.println("用户名已存在！");
        }
    }

    @Test
    public void queryUserByUsernamePassword() {
        if((userDao.queryUserByUsernamePassword("y_eleven","123456"))==null){
            System.out.println("用户名或者密码错误，登录失败！");
        }else {
            System.out.println("查询成功！");
        }
    }

    @Test
    public void saveUser() {
        System.out.println(userDao.saveUser(new User(null,"li_rc","123456","leader.rc@qq.com")) );

    }
}