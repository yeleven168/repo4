package com.yeleven.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author y_eleven
 * @Date 2020/7/24 12:06
 */
public class UserServletTest {
    public void login(){
        System.out.println("这是调用了login（）方法！");
    }
    public void regist(){
        System.out.println("这是调用了regist（）方法！");
    }
    public void updateUser(){
        System.out.println("这是调用了updateUser（）方法！");
    }
    public void updateUserPassword(){
        System.out.println("这是调用了updateUserPassword（）方法！");
    }

    public static void main(String[] args) {
        String action = "updateUserPassword";

        try {
            Method method = UserServletTest.class.getDeclaredMethod(action);
//            System.out.println(method);
            try {
                method.invoke(new UserServletTest());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
