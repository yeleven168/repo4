package com.yeleven.test;


import com.yeleven.utils.JdbcUtils;
import org.junit.Test;

import java.sql.Connection;

/**
 * @author y_eleven
 * @Date 2020/7/21 17:38
 *
 */
public class JdbcUtilsTest {
    @Test
    public void jdbcUtilsTest(){
        for (int i = 0; i <100 ; i++) {
            Connection connection = JdbcUtils.getConnection();
            System.out.println(connection);
            JdbcUtils.close(connection);
        }
    }
}
