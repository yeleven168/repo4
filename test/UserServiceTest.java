package com.yeleven.test;

import com.yeleven.pojo.User;
import com.yeleven.service.UserService;
import com.yeleven.service.impl.UserServiceImpl;
import org.junit.Test;

public class UserServiceTest {
    UserService userService = new UserServiceImpl();

    @Test
    public void registUser() {
        userService.registUser(new User(null,"w_jig","123456","wangjiagui@qq.com"));
        userService.registUser(new User(null,"y_wei","123456","yanwei@qq.com"));
    }

    @Test
    public void login() {
        System.out.println(userService.login(new User(null,"y_eleven","123456",null)));
    }

    @Test
    public void existsUsername() {
        if(userService.existsUsername("y_eleven")) {
            System.out.println("用户名存在！");
        }else {
            System.out.println("用户名可用！");
        }
    }
}